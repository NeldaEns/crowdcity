using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public static UIController ins;
    [HideInInspector] public UIScreenBase currentScreen;
    public GameObject mainMenu;
    public GameObject mainGame;

    private void Awake()
    {
        if(ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
    }


    private void Start()
    {
        currentScreen = Instantiate(mainMenu, transform).GetComponent<UIMenu>();
    }

    public void ShowMenu()
    {
        Destroy(currentScreen.gameObject);
        currentScreen = Instantiate(mainMenu, transform).GetComponent<UIMenu>();
    }

    public void ShowGame()
    {
        Destroy(currentScreen.gameObject);
        currentScreen = Instantiate(mainGame, transform).GetComponent<UIGame>();
    }    
}
