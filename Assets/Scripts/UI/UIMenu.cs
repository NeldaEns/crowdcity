using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.UI;
using UnityEngine.SceneManagement;

public class UIMenu : UIScreenBase
{

    public void OnPlayGame()
    {
        SceneManager.LoadScene(1);
        UIController.ins.ShowGame();
    }

    public override void Hide()
    {
        base.Hide();
    }

    public override void OnShow()
    {
        base.OnShow();
    }
}
